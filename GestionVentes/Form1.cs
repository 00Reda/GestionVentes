﻿using GestionVentes.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionVentes
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalculer_Click(object sender, EventArgs e)
        {
            DB db = new DB();
            var facture = db.Ventes
                .Join(
                db.Articles,
                v => v.ArtId,
                a => a.Id,
                (v, a) => new LigneFacture(a.Id, a.Libelle, a.PU, v.Qte)
                ).ToList();
            grd.DataSource = facture;
            lbCA.Text = facture.Sum(x => x.Prix).ToString();
        }
    }
}
