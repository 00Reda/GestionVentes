﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionVentes.Model
{
    public class Vente
    {
        public int Num { get; set; }
        public int ArtId { get; set; }
        public double Qte { get; set; }
        public Vente()
        {

        }

        public Vente(int num, int artId, double qte)
        {
            Num = num;
            ArtId = artId;
            Qte = qte;
        }
    }
}
