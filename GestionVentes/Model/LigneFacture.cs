﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionVentes.Model
{
    public class LigneFacture
    {
        public LigneFacture(int id, string libelle, double pU, double qte)
        {
            Id = id;
            Libelle = libelle;
            PU = pU;
            Qte = qte;
        }
        public LigneFacture()
        {

        }
        public int Id { get; set; }
        public string Libelle { get; set; }
        public double PU { get; set; }
        public double Qte { get; set; }
        public double Prix { get { return PU * Qte; } }

    }
}
