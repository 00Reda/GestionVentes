﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionVentes.Model
{
    public class DB
    {
        public List<Article> Articles { get; set; }
        public List<Vente>   Ventes   { get; set; }

        public DB()
        {
            #region Creation des Articles
            Articles = new List<Article>();
            Articles.Add(new Article(1,"Pain", 2));
            Articles.Add(new Article(2, "The", 20));
            Articles.Add(new Article(3, "Bananes", 10));
            Articles.Add(new Article(4, "Pomme", 13));
            #endregion
            #region Creation des Ventes
            Ventes = new List<Vente>();
            Ventes.Add(new Vente(1, 1, 10));
            Ventes.Add(new Vente(2, 1, 15));
            Ventes.Add(new Vente(3, 2, 7));
            Ventes.Add(new Vente(4, 1, 8));
            Ventes.Add(new Vente(5, 3, 11));
            Ventes.Add(new Vente(6, 2, 10));
            Ventes.Add(new Vente(7, 2, 20));
            Ventes.Add(new Vente(8, 1, 11));
            #endregion

        }

    }
}
