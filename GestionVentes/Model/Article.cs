﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestionVentes.Model
{
    public class Article
    {
        public int Id { get; set; }
        public string Libelle { get; set; }
        public double PU { get; set; }
        public Article()
        {

        }

        public Article(int id, string libelle, double pU)
        {
            Id = id;
            Libelle = libelle;
            PU = pU;
        }
    }
}
